﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileServe {
    public partial class Form1 : Form {

        // Fields.
        // Keep track of whether we are accepting connections.
        private bool bRunning = false;
        // The socket to listen for connection requests on.
        private Socket socListen = null;
        // A mutex to prevent the rich text box from getting messed up
        // when multiple threads try to access it simultaneously.
        private Mutex mutRTB = null;

        public Form1() {
            InitializeComponent();

            // Create a mutex for guarding the rich text box.
            this.mutRTB = new Mutex();
        }

        private void btnStartStop_Click(object sender, EventArgs e) {
            // If we are already running, stop, otherwise start.
            if (this.bRunning) {
                // Close and dispose of the listening socket.
                this.socListen.Close();
                this.socListen.Dispose();
                // Indicate that we are no longer running and change the
                // text on the start/stop button to be "Start".
                this.bRunning = false;
                this.btnStartStop.Text = "Start";
            } else {
                // Indicate that we are running now and change the text on
                // the start/stop button to be "Stop".
                this.bRunning = true;
                this.btnStartStop.Text = "Stop";
               
                IPAddress ipaLocalHost = IPAddress.Parse("127.0.0.1");
                IPEndPoint ipeListen = new IPEndPoint(ipaLocalHost, 8080);
                socListen = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socListen.Bind(ipeListen);
                socListen.Listen(5);
                
                Task tAcceptConnReq = new Task(vAcceptConnection);
                tAcceptConnReq.Start();
            }
        }
        private delegate void AppendDelegate(string strMsg);

        private void vAcceptConnection()
        {
            for (; ; )
            {
                try
                {
                    Socket socConnection = socListen.Accept(); 
                    Task tHandleConn = new Task(() => vHandleConnection(socConnection));
                    tHandleConn.Start(); 
                }
                catch (Exception)
                {
                    break;
                }
            }
        }

        private void vHandleConnection(Socket socConnection)
        {
            try
            {
                NetworkStream nsSocStream = new NetworkStream(socConnection);
                string strFilePath = ReceiveRequest(nsSocStream);
                string strLog = DateTime.Now.ToString("R") + ": " + socConnection.RemoteEndPoint.ToString() + "\r\n"
                                    + strFilePath;
                mutRTB.WaitOne();
                AppendDelegate adRTB = new AppendDelegate(rtbLog.AppendText);
                this.Invoke(adRTB, strLog + "\r\n");
                mutRTB.ReleaseMutex();
                if (File.Exists(strFilePath))
                {
                    FileStream strmRetriveFile = File.Open(strFilePath, FileMode.Open, FileAccess.Read);
                    byte[] byBuffer = new byte[1024];
                    int iNumBytesRead = 0;
                    while ((iNumBytesRead = strmRetriveFile.Read(byBuffer, 0, byBuffer.Length)) > 0)
                    {
                        nsSocStream.Write(byBuffer, 0, iNumBytesRead);
                    }
                    strmRetriveFile.Close();
                }
                else
                {
                    socConnection.Close();
                    socConnection.Dispose();
                    return;
                }
            }
            catch (Exception)
            {
                return;
            }

        }

        


        // Helper function to read in a file request from the (network) stream.
        private static string ReceiveRequest(Stream strmChannel) {
            // Start with an empty line and append characters one by one as they are
            // read in.
            string strLine = "";
            // The next byte to look at and a "look-ahead" byte, in case the next byte
            // is CR and we need to check whether the following byte is LF.
            int iNextByte;
            int iLookaheadByte;

            // Initialize: The first byte is the next byte and the following byte is
            // the look-ahead byte.
            iNextByte = strmChannel.ReadByte();
            iLookaheadByte = strmChannel.ReadByte();

            // Keep going as long as we haven't received a CR-LF combination yet.
            while (!(iNextByte == '\r' && iLookaheadByte == '\n')) {
                // Append the new character to the line and advance to the next byte.
                strLine += Convert.ToChar(iNextByte);
                iNextByte = iLookaheadByte;
                iLookaheadByte = strmChannel.ReadByte();
            }

            // Received CR-LF, so finished. Return the line.
            return strLine;
        }
    }
}
